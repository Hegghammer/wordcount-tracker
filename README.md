# Wordcount tracker

This is an R script to track the progress of writing projects stored in Gitlab or Github repositories. It uses Rmarkdown to generate a `README.md` file with word count statistics on all the markdown files in the repository (except `toc.md`, `todo.md`, and `README.md` itself). Couple it with continuous integration to get a new `README.md` with updated numbers on each commit or at regular intervals. The script also stores all wordcount observations in a `.csv` file for your records. 

## Sample output

![](illustration.png)

## Installation

1. Clone or download this repository
2. Place `README.Rmd` in the repository that holds your writing project. (Open it and change the `repo_title` variable if you want.)
3. Set up a continuous integration runner to render `README.Rmd` as described below.
4. Push `README.Rmd` to Gitlab/Github.  

## Gitlab CI

For more on Gitlab CI, see the [documentation](https://docs.gitlab.com/ee/ci/). Below is a sample `.gitlab-ci.yml` to autogenerate a `README.md` on each commit. It presupposes that the runner and the repository are set up with SSH keys (details below) allowing the runner to perform commits.

```
image: rocker/verse:4.0.0

stages:
  - commit

build-readme:
  stage: commit
  before_script:
    - "which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )"
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY}" | tr -d '\r' | ssh-add - > /dev/null
    - mkdir -p ~/.ssh 
    - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
    - R -e "install.packages(c('rmarkdown', 'dplyr'))"
  script:
    - git config --global user.email "${CI_GIT_USER_EMAIL}"
    - git config --global user.name "${CI_GIT_USER_USERNAME}"
    - git clone git@gitlab.com:${CI_PROJECT_PATH}.git
    - cd ${CI_PROJECT_NAME}
    - Rscript -e "rmarkdown::render('README.Rmd')"
    - git add .
    - git commit -m "rebuild README.md"
    - git push origin main -o ci.skip
  only:
    - main
```

To set up SSH keys, [these instructions](https://www.iliascreates.com/blog/post/git-commits-from-gitlab-ci/) mostly work. I say mostly because there is a slight problem with the `ssh-keygen` command in the tutorial. Gitlab wants a key generated with the parameter `-m pem`, so use the following command instead: `ssh-keygen -m pem -f deploy_key -N ""`. 

Gitlab is also picky about the formatting of the `SSH_PRIVATE_KEY` variable. Make sure 1) to include the key header and footer (`-----BEGIN RSA PRIVATE KEY-----` and `-----END RSA PRIVATE KEY`), 2) that the dashes in the header and footer come in sequences of exactly five (`-----`),  and 3) to add an empty line at the end of the key. 

## Github CI

I have not implemented the script with CI on Github, but it should be straightforward to do with Github Actions. See the [documentation](https://docs.github.com/en/actions) and the [r-lib/actions](https://github.com/r-lib/actions) page for details.   


     
